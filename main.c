#include <stdio.h> 
#include <stdlib.h>
  
// Funktion zum Einzahlen auf das Konto 
void einzahlen(float betrag_einzahlen, float *guthaben) 
{ 
	printf("\n\n Bitte geben Sie den gewuenschten Betrag ein, den Sie einzahlen moechten:"); 
	scanf("%f", &betrag_einzahlen); 
	*guthaben += betrag_einzahlen;
	printf(" Neuer Kontostand : %.2f Euro\n", *guthaben);
	//Öffnen und schreiben des Kontostands welcher in der Datei.txt gespeichert ist. 
	FILE *fp;
	fp = fopen("konto.txt", "w");
	fprintf(fp,"%.2f\n", *guthaben);
	fclose(fp);
} 

// Funktion zum Auszahlen vom Konto 
void auszahlen(float betrag_auszahlen, float *guthaben) { 
    printf("\n\n Bitte geben Sie den gewuenschten Betrag ein, den Sie abheben moechten:"); 
    scanf("%f", &betrag_auszahlen); 
    *guthaben -= betrag_auszahlen; 
	if(*guthaben < -2500.01) { 
        printf("\n (!)Ihr Konto ist nicht ausreichend gedeckt!\a");
        printf("\n Dieser Betrag ueberschreitet Ihren Dispo von 2500.00 EURO");
    } 
    else { 
    printf("Neuer Kontostand: %.2f Euro\n", *guthaben);
    //Öffnen und schreiben des Kontostands welcher in der Datei.txt gespeichert ist. 
	FILE *fp;
	fp = fopen("konto.txt", "w");
	fprintf(fp,"%.2f\n", *guthaben);
	fclose(fp); 
    } 
} 
// Funktion zum Abrufen des Kontostands 
void kontostand(float *guthaben) { 
    printf("\n\n Kontostand: %.2f Euro\n", *guthaben); 
} 

//Hauptfunktion des Projekts   
int main() { 
	
  //denifieren und initialisieren der benötigten Variablen
    char wiederholen; 
    float guthaben; 
    float betrag_einzahlen; 
    float betrag_auszahlen; 
    int auswahl;
    float i;
    
    printf("\nHerzlich Willkommen bei Ihrer Bank \n");
    printf("_____________________________________________\n\n"); 
    //Übersicht + Abfrage von den möglichen Funktionen mithilfe der puts-Funktion
    do {
    //Öffnen und lesen des Kontostands welcher in der Datei.txt gespeichert ist.	
    FILE*fp;
    fp=fopen("konto.txt", "r");
    if (fp==NULL)
	{
    	printf("Entschuldigen Sie, es ist ein Fehler aufgetreten. Ihr Kontostand konnte nicht geladen werden und das Programm muss beendet werden\n");
		break;	
	}
	else
	{
		printf("(Kontostand wurde erfolgreich geladen!)\n");
	}
    fscanf(fp, "%f\n", &guthaben);
    fclose(fp);
    
    puts("\nWas moechten Sie tun?\n"); 
    puts("1 - Geld einzahlen"); 
    puts("2 - Geld auszahlen"); 
    puts("3 - Kontostand abfragen"); 
    puts("4 - Uebersicht verlassen \n"); 
    printf("Option waehlen:"); 
  //Abfrage der Auswahl einer Funktion über Tastatureingabe mithilfe der switch-case Funktion
    do { 
        scanf("%i", &auswahl); 
  
        switch(auswahl) { 
        case 1: einzahlen(betrag_einzahlen, &guthaben); 
                break; 
        case 2: auszahlen(betrag_auszahlen, &guthaben); 
                break; 
        case 3: kontostand(&guthaben); 
                break; 
        case 4: printf("\n\n > Bitte druecken Sie 2x eine beliebige Taste um das Programm zu beenden \n"); 
                _getch(); 
                return 0; 
                break; 
        default:printf("\n (!) Bitte zwischen 1 - 4 waehlen: \a"); // \a verursacht einen Piepston.
        } 
    } 
	// Wiederholung der 2. do-while schleife bis eine Eingabe zwischen 1-4 erfolgt ist.
	while (auswahl< 1 || auswahl > 4); 
    printf("\n ______________________________\n"); 
    printf("\n Bearbeitung fortsetzen? <ja/nein>\n"); 
    wiederholen=_getch(); 
    printf("\n ______________________________\n"); 
    }
	//wiederholung der 1. do-while Schleife bis die Uebersicht verlassen werden soll.
	while (wiederholen=='j' || wiederholen=='J'); 
   
}