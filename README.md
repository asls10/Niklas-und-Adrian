Projekt Kontoführung - BigMoney

Grundfunktionen eines Geldautomaten.
(1) Einzahlen
(2) Auszahlen
(3) Kontostand ausgeben
(4) Kontostand Speichern

Über das CMD Fenster soll eine Übersicht über die Funktionen gelistet werden, die man dann über die Tastatureingabe aufrufen kann. 
Beispiel:
Was moechten Sie tun: 
(1) Geld einzahlen
(2) ....
Option wählen: (Hier erfolgt die Eingabe über die Tastatur)

Die größte Herausforderung ist es den Kontostand extern zu speichern, sodass nach dem Schließen des Programms der Kontostand wieder geladen wird.
Lösungsansatz: Eventuell gibt es eine Möglichkeit, dass der Kontostand in einer externen Text Datei gespeichert werden kann,
der beim Aufrufen des Programms wieder geladen werden kann.